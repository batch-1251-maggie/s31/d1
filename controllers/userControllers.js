
let express = require('express');
let router = express.Router();
let User = require('./../models/User');


//insert a new user
module.exports.createUser = function(reqBody){

	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		userName: reqBody.userName,
		password: reqBody.password
	})

	return newUser.save().then((savedUser, error) => {
		if(error){
			return error
		} else {
			return savedUser
		}
	})
}

//createUser(req.body);


//retrieve all users
module.exports.getAll = () => {

	return User.find().then((result, error)=>{

		if(error){
			return error
		} else {
			return result
		}
	})
}


//getAll();

//retrieve specific user
module.exports.getUser = (params) => {
	// let params = req.params.id
	return User.findById(params).then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}

//getUser();

//update specific user
module.exports.updateUser = (params, reqBody) => {
	return User.findByIdAndUpdate(params, reqBody, {new:true}).then((result, error) => {
		if(error){
			return error
		} else {
			return result
		}
	})
}

//updateUser();

//delete a specific user
module.exports.deleteUser = (params) => {
	return User.findByIdAndDelete(params).then((result, error) => {
		if(error){
			return false
		} else {
			return true
		}
	})
}

//deleteUser();